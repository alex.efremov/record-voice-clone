import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import Routes from './Routes';
import Header from './components/Header';
import { getAllRecordings } from './actions';

import './App.css';

class App extends React.PureComponent {
  componentDidMount() {
    this.props.getAllRecordings();
  }

  render() {
    return (
      <div>
        <header className='header'>
          <Header />
        </header>
        <main className='main'>
          <Routes />
        </main>
      </div>
    );
  }
}

App.propTypes = {
  getAllRecordings: PropTypes.func
};

export default withRouter(connect(null, { getAllRecordings })(App));
