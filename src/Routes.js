import React from 'react';
import { Route, Switch } from 'react-router-dom';

import RecordView from './containers/RecordView';
import RecordingsView from './containers/RecordingsContainer';

export default () => {
  return (
    <Switch>
      <Route
        component={RecordView}
        exact
        path='/'
      />
      <Route
        component={RecordingsView}
        path='/recordings'
      />
    </Switch>
  );
};
