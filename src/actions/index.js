import storage from '../libs/storage';

export const startRecording = () => ({
  type: 'START_RECORDING'
});

export const stopRecording = () => ({
  type: 'STOP_RECORDING'
});

export const openModal = () => ({
  type: 'OPEN_MODAL'
});

export const closeModal = () => ({
  type: 'CLOSE_MODAL'
});

export const saveRecording = recording => ({
  type: 'SAVE_RECORDING',
  payload: storage.setItem(recording.id, recording),
  meta: {
    payload: recording
  }
});

export const selectRecording = id => ({
  type: 'SELECT_RECORDING',
  id
});

export const getAllRecordings = () => ({
  type: 'GET_ALL_RECORDINGS',
  payload: storage.getAll()
});

export const interruptRecording = () => ({
  type: 'INTERRUPT_RECORDING'
});

export const deleteRecording = id => ({
  type: 'DELETE_RECORDING',
  payload: storage.remove(id),
  meta: {
    id
  }
});

export const finishRedirecting = () => ({
  type: 'FINISH_REDIRECTING'
});

export const updateTitle = (id, title) => ({
  type: 'UPDATE_TITLE',
  payload: storage.updateByKey(id, 'title', title),
  meta: {
    id,
    title
  }
});
