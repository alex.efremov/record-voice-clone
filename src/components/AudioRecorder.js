import React from 'react';
import PropTypes from 'prop-types';

navigator.getUserMedia = (navigator.getUserMedia ||
                          navigator.webkitGetUserMedia ||
                          navigator.mozGetUserMedia ||
                          navigator.msGetUserMedia);

const constraints = { audio: true, video: false };

class AudioRecorder extends React.PureComponent { //eslint-disable-line
  componentDidMount() {
    if (this.props.record) {
      this.handleRecording();
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.record !== this.props.record) {
      this.handleRecording();
    }
  }

  componentWillUnmount() {
    if (this.props.record) {
      this.stopRecording(false);
    }
  }

  mediaRecorder = null;
  mediaStreamTrack = null;
  recordedChunks = [];
  startTime = null;
  stream = null;

  handleRecording = () => {
    if (this.props.record) {
      this.startRecording();
    } else {
      this.stopRecording(true);
    }
  }

  initialize = async () => {
    if (navigator.mediaDevices) {
      try {
        this.stream = await navigator.mediaDevices.getUserMedia(constraints);
        this.mediaStreamTrack = this.stream.getTracks()[0];
        this.mediaRecorder = new MediaRecorder(this.stream, this.props.mediaOptions);
        this.mediaRecorder.addEventListener('dataavailable', this.onDataAvailable);
      }
      catch (e) {
        console.log(new Error(e));
      }
    } else {
      throw new Error('You browser doesn\'t support WebAudioApi.');
    }
  }

  startRecording = async () => {
    await this.initialize();
    if (this.mediaRecorder) {
      this.mediaRecorder.start();
      this.startTime = Date.now();
      if (this.props.onStart) {
        this.props.onStart(this.stream);
      }
    }
  }

  stopRecording = (save) => {
    if (this.mediaRecorder) {
      if (save) {
        this.mediaRecorder.onstop = this.onStopRecording;
      } else {
        this.mediaRecorder.onstop = null;
        console.log('canceled');
      }
      this.mediaRecorder.stop();
      this.mediaStreamTrack.stop();
      this.recordedChunks = [];
    }
  }

  onDataAvailable = e => {
    if (e.data.size > 0) {
      this.recordedChunks.push(e.data);
    }
  }

  onStopRecording = () => {
    const duration = (Date.now() - this.startTime);
    const blob = new Blob(this.recordedChunks, this.props.mediaOptions);
    if (this.props.onStop) {
      this.props.onStop({
        blob,
        createdAt: this.startTime,
        duration
      });
    }
  }

  render() {
    return null;
  }
}

AudioRecorder.propTypes = {
  mediaOptions: PropTypes.object,
  onStart: PropTypes.func,
  onStop: PropTypes.func,
  record: PropTypes.bool
};

AudioRecorder.defaultProps = {
  record: false,
  mediaOptions: { type: 'audio/webm' }
};

export default AudioRecorder;
