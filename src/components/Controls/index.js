import React from 'react';
import { pure } from 'recompose';
import PropTypes from 'prop-types';
import AvMic from 'material-ui/svg-icons/av/mic';
import DeleteForever from 'material-ui/svg-icons/action/delete-forever';
import Done from 'material-ui/svg-icons/action/done';

import './Controls.css';

const BtnStyle = {
  color: 'white'
};

const DoneStyle = {
  color: 'green'
};

const Controls = ({
  isRecording,
  onRecordClick,
  onDoneClick,
  onDeleteClick }) => {
  if (!isRecording) {
    return (
      <div className='Controls'>
        <div
          className='circle-btn-60x60 blue-btn'
          onClick={onRecordClick}
        >
          <AvMic style={BtnStyle} />
        </div>
      </div>
    );
  } else {
    return (
      <div className='Controls'>
        <div
          className='control-icon'
          onClick={onDeleteClick}
        >
          <DeleteForever />
        </div>
        <div className='circle-btn-60x60 blue-btn-disabled'>
          <AvMic style={BtnStyle} />
        </div>
        <div
          className='control-icon'
          onClick={onDoneClick}
        >
          <Done style={DoneStyle} />
        </div>
      </div>
    );
  }

};

Controls.propTypes = {
  isRecording: PropTypes.bool,
  onDeleteClick: PropTypes.func,
  onDoneClick: PropTypes.func,
  onRecordClick: PropTypes.func,
};

export default pure(Controls);
