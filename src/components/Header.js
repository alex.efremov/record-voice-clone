import React from 'react';
import AppBar from 'material-ui/AppBar';
import { Tabs, Tab } from 'material-ui/Tabs';
import AvMic from 'material-ui/svg-icons/av/mic';
import AvPlaylistPlay from 'material-ui/svg-icons/av/playlist-play';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, withHandlers, withProps } from 'recompose';

const mapStateToProps = ({ recording: { count, isRecording } }) => ({
  isRecording,
  count
});

const enhance = compose(
  withRouter,
  connect(mapStateToProps, null),
  withProps(({ history, isRecording }) => ({
    initialSelectedIndex: history.location.pathname === '/' ? 0 : 1,
    tabStyle: isRecording ? { color: 'red' } : null
  })),
  withHandlers({
    handleOnChange: ({ history }) => path => {
      if (history.location.pathname !== path) {
        history.push(path);
      }
    }
  })
);

const appBarStyle = {
  flexWrap: 'wrap'
};

const tabsStyle = {
  flex: '1 1 100%',
  margin: '0 auto'
};

const Header = ({ handleOnChange, initialSelectedIndex, count, tabStyle, history }) => {
  return (
    <AppBar
      showMenuIconButton={false}
      style={appBarStyle}
    >
      <Tabs
        initialSelectedIndex={initialSelectedIndex}
        onChange={handleOnChange}
        style={tabsStyle}
        value={history.location.pathname}
      >
        <Tab
          icon={<AvMic />}
          label='Record'
          style={tabStyle}
          value='/'
        />
        <Tab
          icon={<AvPlaylistPlay />}
          label={`Listen (${count})`}
          value='/recordings'
        />
      </Tabs>
    </AppBar>
  );
};

Header.propTypes = {
  count: PropTypes.number,
  handleOnChange: PropTypes.func,
  initialSelectedIndex: PropTypes.number,
  tabStyle: PropTypes.object
};

export default enhance(Header);
