import React from 'react';
import { compose, pure, branch, renderNothing, lifecycle, withHandlers } from 'recompose';
import RaisedButton from 'material-ui/RaisedButton';
import PropTypes from 'prop-types';

import './Modal.css';

const enhance = compose(
  pure,
  branch(({ isOpen }) => !isOpen, renderNothing),
  withHandlers({
    handleOnOuterDivClick: ({ onNoClick }) => e => {
      if (e.target.className === 'Modal' && onNoClick) {
        onNoClick();
      }
    }
  }),
  lifecycle({
    componentDidMount() {
      document.body.style.overflow = 'hidden';
    },
    componentWillUnmount() {
      document.body.style.overflow = '';
    }
  })
);

const Modal = ({ onNoClick, onYesClick, text, handleOnOuterDivClick }) => {
  return (
    <div
      className="Modal"
      onClick={handleOnOuterDivClick}
    >
      <div className="Modal-content">
        <p>{text}</p>
        <div className='Modal-buttons'>
          <div className='Modal-button-container'>
            <RaisedButton
              label='Yes'
              onTouchTap={onYesClick}
              secondary
            />
          </div>
          <div className='Modal-button-container'>
            <RaisedButton
              label='No'
              onTouchTap={onNoClick}
              primary
            />
          </div>
        </div>
      </div>
    </div>
  );
};

Modal.propTypes = {
  handleOnOuterDivClick: PropTypes.func,
  isOpen: PropTypes.bool, //eslint-disable-line
  onNoClick: PropTypes.func,
  onYesClick: PropTypes.func,
  text: PropTypes.string
};

export default enhance(Modal);
