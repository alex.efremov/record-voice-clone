import React from 'react';
import PropTypes from 'prop-types';

const audioCtx = new (window.AudioContext || window.webkitAudioContext)();

class Oscilloscope extends React.PureComponent { //eslint-disable-line
  componentDidMount() {
    this.ctx = this.canvas.getContext('2d');
    this.ctx.strokeStyle = this.props.lineColor;
    this.ctx.lineWidth = this.props.lineWidth;
    this.cnvsHalfHeight = this.canvas.height / 2;
    this.analyser = audioCtx.createAnalyser();
    this.analyser.fftSize = 2048;
    this.bufferLength = this.analyser.frequencyBinCount;
    if (this.props.stream) {
      this.setupNewStream();
      if (this.props.shouldVisualize) {
        this.handleVisualization();
        return;
      }
    }
    this.resetCanvas();
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (nextProps.stream !== this.props.stream ||
      nextProps.shouldVisualize !== this.props.shouldVisualize) {
      return true;
    } else {
      return false;
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.stream && this.props.stream !== prevProps.stream) {
      this.setupNewStream();
    }
    this.handleVisualization();
  }

  componentWillUnmount() {
    if (this.isVisualizing) {
      cancelAnimationFrame(this.drawVisual);
    }
  }

  ctx = null;
  cnvsHalfHeight = null;
  dataArray = [];
  drawVisual = null;
  analyser = null;
  bufferLength = null;
  isVisualizing = false;

  handleVisualization = () => {
    const { stream, shouldVisualize } = this.props;
    if (!stream) {
      if (this.isVisualizing) {
        this.resetVisualization();
      }
    } else if (shouldVisualize && !this.isVisualizing &&
      this.props.stream.getTracks()[0].readyState === 'live') {
      this.visualize();
    } else if (!shouldVisualize && this.isVisualizing) {
      this.resetVisualization();
    }
  }

  setupNewStream = () => {
    const source = audioCtx.createMediaStreamSource(this.props.stream);
    source.connect(this.analyser);
    this.dataArray = new Uint8Array(this.bufferLength);
  }

  clearCanvas = () => {
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
  }

  resetCanvas = () => {
    this.clearCanvas();
    this.ctx.beginPath();
    this.ctx.moveTo(0, this.cnvsHalfHeight);
    this.ctx.lineTo(this.canvas.width, this.cnvsHalfHeight);
    this.ctx.stroke();
  }

  drawOscilloscope = () => {
    if (this.props.stream.getTracks()[0].readyState === 'ended') {
      this.resetVisualization();
    } else {
      this.drawVisual = requestAnimationFrame(this.drawOscilloscope);
      this.analyser.getByteTimeDomainData(this.dataArray);
      this.clearCanvas();
      this.ctx.beginPath();
      var sliceWidth = this.canvas.width * 1.0 / this.bufferLength;
      var x = 0;
      for (var i = 0; i < this.bufferLength; i++) {
        var v = this.dataArray[i] / 128.0;
        var y = v * this.cnvsHalfHeight;
        if (i === 0) {
          this.ctx.moveTo(x, y);
        } else {
          this.ctx.lineTo(x, y);
        }
        x += sliceWidth;
      }
      this.ctx.lineTo(this.canvas.width, this.cnvsHalfHeight);
      this.ctx.stroke();
    }
  }

  visualize = () => {
    this.drawOscilloscope();
    this.isVisualizing = true;
  }

  resetVisualization = () => {
    cancelAnimationFrame(this.drawVisual);
    this.resetCanvas();
    this.isVisualizing = false;
  }

  render() {
    return (
      <div style={this.props.oscilloscopeStyle}>
        <canvas
          ref={n => this.canvas = n}
          style={this.props.canvasStyle}
        />
      </div>
    );
  }
};

Oscilloscope.propTypes = {
  canvasStyle: PropTypes.object,
  lineColor: PropTypes.string,
  lineWidth: PropTypes.number,
  oscilloscopeStyle: PropTypes.object,
  shouldVisualize: PropTypes.bool,
  stream: PropTypes.object
};

Oscilloscope.defaultProps = {
  lineColor: 'black',
  lineWidth: 2,
  shouldVisualize: false
};

export default Oscilloscope;
