import React from 'react';
import { pure } from 'recompose';
import PropTypes from 'prop-types';
import AvMic from 'material-ui/svg-icons/av/mic';

import './Recorder.css';
import AudioRecorder from '../AudioRecorder';
import Oscilloscope from '../Oscilloscope';
import Modal from '../Modal';
import Controls from '../Controls';
import Timer from '../Timer';

const osclilloscopeStyle = {
  height: '200px',
  width: '100%',
  borderRadius: '10px'
};

const canvasStyle = {
  boxShadow: '2px 2px 1px #888888',
  borderRadius: '10px',
  height: '100%',
  width: '100%',
  backgroundColor: 'rgb(255, 64, 129)'
};

const AvMicStyle = {
  width: '100%',
  height: '100%'
};

const Recorder = ({
  isOpen, isRecording, stream, onRecordStart, onRecordStop,
  modalText, onDeleteClick, onDoneClick, onNoClick, onRecordClick,
  onYesClick }) => {
  return (
    <div className='Recorder'>
      <div className='mic-icon'>
        <AvMic style={AvMicStyle} />
      </div>
      <div className='Oscilloscope-container'>
        <Oscilloscope
          canvasStyle={canvasStyle}
          lineColor='rgb(0, 188, 212)'
          oscilloscopeStyle={osclilloscopeStyle}
          shouldVisualize={isRecording}
          stream={stream}
        />
      </div>
      <div className='timer-container'>
        <Timer count={isRecording} />
      </div>
      <Controls
        isRecording={isRecording}
        onDeleteClick={onDeleteClick}
        onDoneClick={onDoneClick}
        onRecordClick={onRecordClick}
      />
      <div>
        <Modal
          isOpen={isOpen}
          onNoClick={onNoClick}
          onYesClick={onYesClick}
          text={modalText}
        />
        <AudioRecorder
          onStart={onRecordStart}
          onStop={onRecordStop}
          record={isRecording}
        />
      </div>
    </div>
  );
};

Recorder.propTypes = {
  isOpen: PropTypes.bool,
  isRecording: PropTypes.bool,
  modalText: PropTypes.string,
  onDeleteClick: PropTypes.func,
  onDoneClick: PropTypes.func,
  onNoClick: PropTypes.func,
  onRecordClick: PropTypes.func,
  onRecordStart: PropTypes.func,
  onRecordStop: PropTypes.func,
  onYesClick: PropTypes.func,
  stream: PropTypes.object
};

export default pure(Recorder);
