import React from 'react';
import PropTypes from 'prop-types';
import { compose, branch, renderNothing } from 'recompose';
import KbBackspace from 'material-ui/svg-icons/hardware/keyboard-backspace';
import DeleteForever from 'material-ui/svg-icons/action/delete-forever';
import ModeEdit from 'material-ui/svg-icons/editor/mode-edit';
import FileDownload from 'material-ui/svg-icons/file/file-download';
import PlayArrow from 'material-ui/svg-icons/av/play-arrow';
import Pause from 'material-ui/svg-icons/av/pause';


import WaveSurferDisplay from './WaveSurferDisplay';

const svgStyle = {
  width: '30px',
  height: '30px'
};

const btnStyle = {
  color: 'white'
};

const enhance = compose(
  branch(({ selectedRecording }) => !selectedRecording, renderNothing)
);

const RecordingDetailsDisplay = ({
  onResetRecording,
  selectedRecording,
  onDeleteClick,
  onEditClick,
  onFinish,
  onPlayingClick,
  isPlaying }) => (
    <div className='recording-details'>
      <div className='recording-details-controls'>
        <div className='recording-details-controls-left'>
          <div
            className='btn'
            onClick={onResetRecording}
          >
            <KbBackspace style={svgStyle} />
          </div>
        </div>
        <div className='recording-details-controls-right'>
          <a
            className='btn'
            download={`${selectedRecording.title}.webm`}
            href={URL.createObjectURL(selectedRecording.blob)}
          >
            <FileDownload style={svgStyle} />
          </a>
          <div
            className='btn'
            onClick={onEditClick}
          >
            <ModeEdit style={svgStyle} />
          </div>
          <div
            className='btn'
            onClick={onDeleteClick}
          >
            <DeleteForever style={svgStyle} />
          </div>
        </div>
      </div>
      <div className='recording-details-title'>
        <h4>{selectedRecording.title}</h4>
      </div>
      <WaveSurferDisplay
        blob={selectedRecording.blob}
        cursorColor={'rgb(255, 64, 129)'}
        onFinish={onFinish}
        play={isPlaying}
        progressColor={'rgb(0,66,75)'}
        waveColor={'rgb(0, 188, 212)'}
      />
      <div className='recording-details-audioplayer'>
        <div
          className='circle-btn-60x60 blue-btn'
          onClick={onPlayingClick}
        >
          {!isPlaying ? (
            <PlayArrow style={btnStyle} />
          ) : (
            <Pause style={btnStyle} />
          )}
        </div>
      </div>
    </div>
);

RecordingDetailsDisplay.propTypes = {
  isPlaying: PropTypes.bool,
  onDeleteClick: PropTypes.func,
  onEditClick: PropTypes.func,
  onFinish: PropTypes.func,
  onPlayingClick: PropTypes.func,
  onResetRecording: PropTypes.func,
  selectedRecording: PropTypes.object,
};

export default enhance(RecordingDetailsDisplay);
