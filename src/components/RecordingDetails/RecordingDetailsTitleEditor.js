import React from 'react';
import PropTypes from 'prop-types';
import KbBackspace from 'material-ui/svg-icons/hardware/keyboard-backspace';
import Done from 'material-ui/svg-icons/action/done';
import TextField from 'material-ui/TextField';
import { compose, withState, withHandlers } from 'recompose';

const svgStyle = {
  width: '30px',
  height: '30px'
};

const enhance = compose(
  withState('value', 'setValue', ({ title }) => title),
  withHandlers({
    handleOnChange: ({ setValue }) => (e, newValue) => setValue(newValue),
    onDoneClick: ({ onDoneClick, value }) => () => onDoneClick(value)
  })
);

const RecordingDetailsTitleEditor = ({
  handleOnChange,
  onDoneClick,
  onResetRecording,
  value
 }) => (
   <div className='recording-details'>
     <div className='recording-details-controls'>
       <div className='recording-details-controls-left'>
         <div
           className='btn'
           onClick={onResetRecording}
         >
           <KbBackspace style={svgStyle} />
         </div>
       </div>
       <div
         className='recording-details-controls-right'
         onClick={onDoneClick}
         style={{ justifyContent: 'flex-end' }}
       >
         <Done style={svgStyle} />
       </div>
     </div>
     <TextField
       autoFocus
       name='title'
       onChange={handleOnChange}
       value={value}
     />
   </div>
);

RecordingDetailsTitleEditor.propTypes = {
  handleOnChange: PropTypes.func,
  onDoneClick: PropTypes.func,
  onResetRecording: PropTypes.func,
  title: PropTypes.string, //eslint-disable-line
  value: PropTypes.string
};

export default enhance(RecordingDetailsTitleEditor);
