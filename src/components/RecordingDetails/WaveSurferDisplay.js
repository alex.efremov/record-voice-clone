import React from 'react';
import PropTypes from 'prop-types';

const WaveSurfer = window.WaveSurfer;

class WaveSurferDisplay extends React.PureComponent {
  static propTypes = {
    blob: PropTypes.object.isRequired,
    cursorColor: PropTypes.string,
    onFinish: PropTypes.func,
    play: PropTypes.bool,
    progressColor: PropTypes.string,
    waveColor: PropTypes.string
  }

  componentDidMount() {
    const {
      blob,
      cursorColor,
      progressColor,
      waveColor,
      onFinish } = this.props;

    this.waveSurfer = WaveSurfer.create({
      container: this.waveForm,
      cursorColor: cursorColor,
      progressColor: progressColor,
      waveColor: waveColor,
      fillParent: true,
      hideScrollbar: true,
      barHeight: 1.2,
      pixelRatio: 1,
      normalize: true
    });
    this.waveSurfer.loadBlob(blob);
    this.waveSurfer.on('finish', () => {
      this.waveSurfer.stop();
      if (onFinish) {
        onFinish();
      }
    });
  }

  componentDidUpdate(prevProps) {
    const wasPlaying = prevProps.play;
    const { play: isPlaying } = this.props;
    if (!wasPlaying && isPlaying) {
      this.waveSurfer.play();
    } else if (wasPlaying && !isPlaying) {
      this.waveSurfer.pause();
    }
  }

  componentWillUnmount() {
    this.waveSurfer.destroy();
  }

  render() {
    return (
      <div className='wave-form'>
        <div ref={n => this.waveForm = n} />
      </div>
    );
  }
};

export default WaveSurferDisplay;
