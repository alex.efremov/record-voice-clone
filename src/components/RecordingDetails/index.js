import React from 'react';
import Drawer from 'material-ui/Drawer';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { compose, withState, withHandlers } from 'recompose';

import RecordingDetailsDisplay from './RecordingDetailsDisplay';
import RecordingDetailsTitleEditor from './RecordingDetailsTitleEditor';
import Modal from '../Modal';
import './RecordingDetails.css';
import { openModal, closeModal, deleteRecording, updateTitle } from '../../actions';

const mapStateToProps = ({ ui }) => ({
  isOpen: ui.isOpen
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  handleOnDeleteClick: () => dispatch(openModal()),
  handleOnNoClick: () => dispatch(closeModal()),
  handleOnYesClick: () =>
    dispatch(deleteRecording(ownProps.selectedRecording.id)),
  handleOnDoneClick: title =>
    dispatch(updateTitle(ownProps.selectedRecording.id, title))
});

const enhance = compose(
  connect(mapStateToProps, mapDispatchToProps),
  withState('isPlaying', 'setIsPlaying', false),
  withState('isEditing', 'setIsEditing', false),
  withHandlers({
    onResetRecording: ({ onResetRecording, setIsPlaying, setIsEditing }) => () => {
      onResetRecording();
      setIsPlaying(false);
      setIsEditing(false);
    },
    handleOnFinish: ({ setIsPlaying }) => () => setIsPlaying(false),
    handleOnPlayingClick: ({ setIsPlaying }) => () => setIsPlaying(n => !n),
    handleOnEditClick: ({ setIsEditing, setIsPlaying }) => () => {
      setIsEditing(true);
      setIsPlaying(false);
    },
    handleOnDoneClick: ({ handleOnDoneClick, setIsEditing }) => title => {
      handleOnDoneClick(title);
      setIsEditing(false);
    }
  })
);

const RecordingDetails = ({
  selectedRecording,
  onResetRecording,
  handleOnDeleteClick,
  handleOnDoneClick,
  handleOnEditClick,
  handleOnFinish,
  handleOnNoClick,
  handleOnYesClick,
  handleOnPlayingClick,
  isEditing,
  isPlaying,
  isOpen
 }) => (
   <div>
     <Modal
       isOpen={isOpen && !!selectedRecording}
       onNoClick={handleOnNoClick}
       onYesClick={handleOnYesClick}
       text={`Do you want to delete this recording?`}
     />
     <Drawer
       containerClassName='right-drawer'
       disableSwipeToOpen
       docked={false}
       onRequestChange={onResetRecording}
       open={selectedRecording ? true : false}
       openSecondary
       width={500}
     >
       {!isEditing ? (
         <RecordingDetailsDisplay
           isPlaying={isPlaying}
           onDeleteClick={handleOnDeleteClick}
           onEditClick={handleOnEditClick}
           onFinish={handleOnFinish}
           onPlayingClick={handleOnPlayingClick}
           onResetRecording={onResetRecording}
           selectedRecording={selectedRecording}
         />
       ) : (
         <RecordingDetailsTitleEditor
           onDoneClick={handleOnDoneClick}
           onResetRecording={onResetRecording}
           title={selectedRecording.title}
         />
       )}
     </Drawer>
   </div>
);

RecordingDetails.propTypes = {
  handleOnDeleteClick: PropTypes.func,
  handleOnDoneClick: PropTypes.func,
  handleOnEditClick: PropTypes.func,
  handleOnFinish: PropTypes.func,
  handleOnNoClick: PropTypes.func,
  handleOnPlayingClick: PropTypes.func,
  handleOnYesClick: PropTypes.func,
  isEditing: PropTypes.bool,
  isOpen: PropTypes.bool,
  isPlaying: PropTypes.bool,
  onResetRecording: PropTypes.func,
  selectedRecording: PropTypes.object
};

export default enhance(RecordingDetails);
