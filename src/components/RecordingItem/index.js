import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, withProps, withHandlers } from 'recompose';
import dateFns from 'date-fns';

import { convertTime, formatBytes } from '../../libs/converters';
import { selectRecording } from '../../actions';
import './RecordingItem.css';

const withKey = propName => Component => props => (
  <Component
    key={props[propName]} //eslint-disable-line
    {...props}
  />
);

const enhance = compose(
  withKey('id'),
  connect(null, { selectRecording }),
  withProps(({ duration, blob }) => ({
    duration: convertTime(duration),
    size: formatBytes(blob.size)
  })),
  withHandlers({
    handleOnClick: ({ selectRecording, id }) => e => {
      selectRecording(id);
    }
  })
);

const RecordingItem = ({ id, title, duration, size, handleOnClick, createdAt }) => (
  <li
    className='recording-item'
    onClick={handleOnClick}
  >
    <div>
      <h4>{title}</h4>
      <p className='date'>{dateFns.format(createdAt, 'MMMM D YYYY, h:mm a')}</p>
    </div>
    <div className='right'>
      <p>{duration}</p>
      <p>{size}</p>
    </div>
  </li>
);

RecordingItem.propTypes = {
  createdAt: PropTypes.number,
  duration: PropTypes.string,
  handleOnClick: PropTypes.func,
  id: PropTypes.string,
  size: PropTypes.string,
  title: PropTypes.string,
};

export default enhance(RecordingItem);
