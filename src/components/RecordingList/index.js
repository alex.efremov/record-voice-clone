import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { branch, renderComponent } from 'recompose';

import RecordingItem from '../RecordingItem';
import RecordingDetails from '../RecordingDetails';
import './RecordingList.css';

const NoRecordings = () => (
  <div className='no-recordings'>
    <h2>No recordings yet.</h2>
  </div>
);

const enhance = branch(
  ({ recordings }) => Object.keys(recordings).length === 0,
  renderComponent(NoRecordings)
);

const RecordingList = ({ recordings, selectedRecording, onResetRecording }) => (
  <div>
    <ul className='recording-list'>
      {_.map(recordings, RecordingItem)}

    </ul>
    <RecordingDetails
      onResetRecording={onResetRecording}
      selectedRecording={selectedRecording}
    />
  </div>
);

RecordingList.propTypes = {
  onResetRecording: PropTypes.func,
  recordings: PropTypes.object,
  selectedRecording: PropTypes.object
};

export default enhance(RecordingList);
