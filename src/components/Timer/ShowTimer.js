import React from 'react';

import { convertTime } from '../../libs/converters';

class ShowTimer extends React.PureComponent {
  constructor() {
    super();
    this.state = {
      time: 0
    };
  }

  componentDidMount() {
    this.timer = setInterval(this.tick, 1000);
  }

  componentWillUnmount() {
    clearInterval(this.timer);
  }

  startTime = Date.now();
  timer = null;

  tick = () => this.setState({ time: Date.now() - this.startTime });

  render() {
    return (
      <div {...this.props} >{convertTime(this.state.time)}</div>
    );
  }
};

export default ShowTimer;
