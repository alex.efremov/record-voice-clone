import React from 'react';

const ShowZeros = (props) => {
  return (
    <div {...props}>00:00</div>
  );
};

export default ShowZeros;
