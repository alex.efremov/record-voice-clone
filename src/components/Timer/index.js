//inspired by https://github.com/rogermarkussen/react.timer
import React from 'react';
import PropTypes from 'prop-types';

import ShowTimer from './ShowTimer';
import ShowZeros from './ShowZeros';

const Timer = ({ count, className }) => {
  if (count) {
    return <ShowTimer className={className} />;
  } else {
    return <ShowZeros className={className} />;
  }
};

Timer.propTypes = {
  className: PropTypes.string,
  count: PropTypes.bool
};

Timer.defaultProps = {
  className: 'timer'
};

export default Timer;
