import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router';
import PropTypes from 'prop-types';

import {
  startRecording,
  stopRecording,
  openModal,
  closeModal,
  interruptRecording,
  saveRecording
} from '../actions';
import Recorder from '../components/Recorder';

const mapStateToProps = ({ recording, ui }) => ({
  count: recording.count,
  isOpen: ui.isOpen,
  isRecording: recording.isRecording,
  redirectTo: ui.redirectTo
});

class RecordView extends React.PureComponent {
  constructor() {
    super();
    this.state = {
      stream: null
    };
  }

  componentWillUnmount() {
    this.cancelRecording();
  }

  shouldSave = true;

  cancelRecording = () => {
    if (this.props.isRecording) {
      this.props.interruptRecording();
    }
  }

  handleOnNoClick = () => {
    this.props.closeModal();
  }

  handleOnYesClick = () => {
    this.shouldSave = false;
    this.props.interruptRecording();
    this.setState({
      steam: null
    });
  }

  handleOnStart = stream => this.setState({
    stream
  })

  handleOnStop = ({ blob, createdAt, duration }) => {
    if (!this.shouldSave) {
      this.shouldSave = true;
    } else {
      const _recording = {
        id: Date.now().toString(),
        createdAt,
        title: `My recording #${this.props.count+1}`,
        blob,
        duration,
      };
      this.props.saveRecording(_recording);
    }
    this.setState({
      stream: null
    });
  }

  handleOnDeleteClick = () => this.props.openModal();

  handleOnDoneClick = () => this.props.stopRecording();

  handleOnRecordClick = () => this.props.startRecording();

  render() {
    const { redirectTo, isOpen, isRecording } = this.props;
    if (redirectTo) {
      return (
        <Redirect to={redirectTo} />
      );
    } else {
      return (
        <Recorder
          isOpen={isOpen}
          isRecording={isRecording}
          modalText='Do you want stop and delete recording?'
          onDeleteClick={this.handleOnDeleteClick}
          onDoneClick={this.handleOnDoneClick}
          onNoClick={this.handleOnNoClick}
          onRecordClick={this.handleOnRecordClick}
          onRecordStart={this.handleOnStart}
          onRecordStop={this.handleOnStop}
          onYesClick={this.handleOnYesClick}
          stream={this.state.stream}
        />

      );
    }
  }
}

RecordView.propTypes = {
  closeModal: PropTypes.func,
  count: PropTypes.number,
  interruptRecording: PropTypes.func,
  isOpen: PropTypes.bool,
  isRecording: PropTypes.bool,
  openModal: PropTypes.func,
  redirectTo: PropTypes.string,
  saveRecording: PropTypes.func,
  startRecording: PropTypes.func,
  stopRecording: PropTypes.func,
};

export default connect(mapStateToProps, {
  startRecording,
  stopRecording,
  openModal,
  closeModal,
  interruptRecording,
  saveRecording
})(RecordView);
