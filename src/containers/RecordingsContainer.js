import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import RecordingList from '../components/RecordingList';
import { selectRecording, finishRedirecting } from '../actions';

const mapStateToProps = ({ recording: { recordings, selectedRecording }, ui }) => ({
  recordings: recordings,
  selectedRecording: recordings[selectedRecording],
  redirectTo: ui.redirectTo
});

const mapDispatchToProps = dispatch => ({
  handleOnResetRecording: () => dispatch(selectRecording(null)),
  finishRedirecting: () => dispatch(finishRedirecting())
});

class RecordingsView extends React.PureComponent {
  componentDidMount() {
    if (this.props.redirectTo === '/recordings') {
      this.props.finishRedirecting();
    }
  }

  componentWillUnmount() {
    this.props.handleOnResetRecording();
  }

  render() {
    const {
      recordings,
      selectedRecording,
      handleOnResetRecording,
      handleOnSelectRecording } = this.props;
    return (
      <RecordingList
        onResetRecording={handleOnResetRecording}
        onSelect={handleOnSelectRecording}
        recordings={recordings}
        selectedRecording={selectedRecording}
      />
    );
  }
};

RecordingsView.propTypes = {
  finishRedirecting: PropTypes.func,
  handleOnResetRecording: PropTypes.func,
  handleOnSelectRecording: PropTypes.func,
  recordings: PropTypes.object,
  redirectTo: PropTypes.string,
  selectedRecording: PropTypes.object
};

export default connect(mapStateToProps, mapDispatchToProps)(RecordingsView);
