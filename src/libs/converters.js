export function convertTime (mSec) {
  const seconds = Math.round(mSec / 1000);
  let minutes = Math.floor(seconds / 60);
  let sec = seconds % 60;
  minutes = minutes < 10 ? `0${minutes}` : `${minutes}`;
  sec = sec < 10 ? `0${sec}` : `${sec}`;
  return `${minutes}:${sec}`;
};


// good impementation from
// https://stackoverflow.com/questions/15900485/correct-way-to-convert-size-in-bytes-to-kb-mb-gb-in-javascript
export function formatBytes(bytes,decimals) {
  if (bytes === 0) {
    return '0 Bytes';
  }
  const k = 1000,
    dm = decimals || 2,
    sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
    i = Math.floor(Math.log(bytes) / Math.log(k));
  return `${parseFloat((bytes / Math.pow(k, i)).toFixed(dm))} ${sizes[i]}`;
};
