import localforage from 'localforage';

localforage.config({
  name: 'RecordVoiceProData',
  driver: [
    localforage.INDEXEDDB,
    localforage.LOCALSTORAGE
  ]
});

const setItem = (id, item) => localforage.setItem(id, item);

const updateByKey = (id, key, value) => {
  return localforage.getItem(id)
  .then(item => {
    const newItem = {
      ...item,
      [key]: value
    };
    return localforage.setItem(id, newItem);
  });
};

const remove = id => localforage.removeItem(id);

const getAll = () => {
  return localforage.keys().then(keys => {
    keys.sort((a, b) => a - b);
    const promises = keys.map(key => localforage.getItem(key));
    return Promise.all(promises);
  });
};

export default {
  getAll,
  remove,
  setItem,
  updateByKey
};
