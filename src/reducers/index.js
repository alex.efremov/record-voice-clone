import { combineReducers } from 'redux';

import recording from './recording-reducer';
import ui from './ui-reducer';

const rootReducer = combineReducers({
  recording,
  ui
});

export default rootReducer;
