import _ from 'lodash';

const INITIAL_STATE = {
  isRecording: false,
  recordings: {},
  selectedRecording: null,
  count: 0
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'START_RECORDING':
      return {
        ...state,
        isRecording: true
      };
    case 'INTERRUPT_RECORDING':
    case 'STOP_RECORDING':
      return {
        ...state,
        isRecording: false
      };
    case 'SAVE_RECORDING_FULFILLED':
      return {
        ...state,
        recordings: {
          ...state.recordings,
          [action.payload.id]: action.payload
        },
        count: state.count+1,
        selectedRecording: action.payload.id
      };
    case 'GET_ALL_RECORDINGS_FULFILLED':
      return {
        ...state,
        recordings: _.mapKeys(action.payload, 'id'),
        count: action.payload.length
      };
    case 'SELECT_RECORDING':
      return {
        ...state,
        selectedRecording: action.id
      };
    case 'DELETE_RECORDING_PENDING':
      return {
        ...state,
        selectedRecording: null,
        count: state.count-1,
        recordings: _.omit(state.recordings, action.meta.id)
      };
    case 'UPDATE_TITLE_PENDING':
      const { id, title } = action.meta;
      return {
        ...state,
        recordings: {
          ...state.recordings,
          [id]: {
            ...state.recordings[id],
            title
          }
        }
      };
    default:
      return state;
  }
};
