const INITIAL_STATE = {
  isOpen: false,
  redirectTo: ''
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'OPEN_MODAL':
      return {
        ...state,
        isOpen: true
      };
    case 'INTERRUPT_RECORDING':
    case 'CLOSE_MODAL':
    case 'DELETE_RECORDING_PENDING':
      return {
        ...state,
        isOpen: false
      };
    case 'SAVE_RECORDING_FULFILLED':
      return {
        ...state,
        redirectTo: '/recordings'
      };
    case 'FINISH_REDIRECTING':
      return {
        ...state,
        redirectTo: ''
      };
    default:
      return state;
  }
};
